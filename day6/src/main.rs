use std::{
    fs,
    collections::{VecDeque, HashMap}
};

const CORRECT_ANSWER: usize = 2334;
const INPUT_PATH: &str = "./input.txt";

#[derive(Debug)]
pub enum AdventError {
    IOError(std::io::Error),
    InputError
}

fn main() -> Result<(), AdventError> {
    let input = fs::read_to_string(INPUT_PATH).map_err(|err| AdventError::IOError(err))?;

    let mut marker = 14;
    let chars: Vec<char> = input.chars().collect();
    let mut queue = VecDeque::from(chars[0..marker].to_vec());

    while !check_marker(&queue) && marker < chars.len() {
        queue.pop_front();
        queue.push_back(chars[marker]);
        marker += 1;
    }

    assert_eq!(CORRECT_ANSWER, marker);
    Ok(())
}

fn check_marker(queue: &VecDeque<char>) -> bool {
    let mut map: HashMap<char, u32> = HashMap::new();

    for c in queue {
        match map.get(&c) {
            Some(value) => map.insert(*c, value + 1),
            None => map.insert(*c, 1)
        };
    }

    !map.into_iter().any(|entry| entry.1 > 1)
}
