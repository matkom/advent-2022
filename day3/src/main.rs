use std::fs::File;
use std::io::{BufRead, BufReader};

const INPUT_PATH: &'static str = "./input.txt";
const CORRECT_ANSWER: u32 = 2817;

type Item = char;
type ItemPriority = u32;

fn main() {
    let mut result: u32 = 0;

    let file = File::open(INPUT_PATH).expect("[ERROR] Failed reading input file");
    let lines = BufReader::new(file).lines();

    let mut group_items = Vec::new();
    for line in lines {
        let text = line.expect("[ERROR] Failed reading line");

        if group_items.len() < 3 {
           group_items.push(text);
           if group_items.len() != 3 {
               continue;
           }
        }

        let first_compartment_items = group_items[0].chars().collect();
        for item in get_item_types(&first_compartment_items) {
            if group_items[1].contains(item) && group_items[2].contains(item) {
                result += get_item_priority(item)
            }
        }

        group_items.clear();
    }

    assert_eq!(CORRECT_ANSWER, result);
    println!("Sum of priorities of items: {}", result);
}

fn get_item_types(items: &Vec<Item>) -> Vec<Item> {
    let mut item_types: Vec<Item> = Vec::new();
    for item in items {
        if item_types.contains(&item) {
            continue;
        }
        item_types.push(*item);
    }
    item_types
}

fn get_item_priority(item: Item) -> ItemPriority {
    if item.is_uppercase() {
        item as u32 - 38
    } else {
        item as u32 - 96
    }
}
