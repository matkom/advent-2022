pub type TreesGrid = Vec<Vec<u32>>;

fn parse(input: &str) -> Result<TreesGrid, String> {
    input
        .lines()
        .map(|s| {
            s.chars()
                .map(|c| c.to_digit(10).ok_or(format!("{} is not a digit", c)))
                .collect()
        })
        .collect::<Result<TreesGrid, String>>()
}

fn get_trees_in_path(tree_coords: (usize, usize), trees_grid: &TreesGrid) -> [Vec<u32>; 4] {
    let row = &trees_grid[tree_coords.0];
    let col = trees_grid
        .iter()
        .map(|row| row[tree_coords.1])
        .collect::<Vec<u32>>();

    let left = row[..tree_coords.1].iter().copied().rev().collect();
    let right = row[tree_coords.1 + 1..].to_vec();
    let up = col[..tree_coords.0].iter().copied().rev().collect();
    let down = col[tree_coords.0 + 1..].to_vec();

    [left, right, up, down]
}

pub fn part_one() -> Result<usize, String> {
    let trees_grid = parse(include_str!("../input.txt"))?;
    let mut visible_trees = (trees_grid.len() - 1) * 4;

    for y in 1..trees_grid.len() - 1 {
        for x in 1..trees_grid.len() - 1 {
            let trees_in_path = get_trees_in_path((y, x), &trees_grid);
            let is_visible = trees_in_path
                .iter()
                .any(|trees| trees.iter().all(|height| *height < trees_grid[y][x]));

            if is_visible {
                visible_trees += 1;
            }
        }
    }

    Ok(visible_trees)
}

pub fn part_two() -> Result<usize, String> {
    let trees_grid = parse(include_str!("../input.txt"))?;

    let mut scenic_scores = vec![];
    for y in 1..trees_grid.len() - 1 {
        for x in 1..trees_grid.len() - 1 {
            let trees_in_path = get_trees_in_path((y, x), &trees_grid);
            let scenic_score = trees_in_path
                .iter()
                .map(|trees| {
                    trees
                        .iter()
                        .position(|tree| *tree >= trees_grid[y][x])
                        .map(|tree_index| tree_index + 1)
                        .unwrap_or(trees.len())
                })
                .product::<usize>();

            scenic_scores.push(scenic_score);
        }
    }

    let max_scenic_score = scenic_scores
        .iter()
        .max()
        .ok_or("Did not calculate any scenic scores. Check input.")?;

    Ok(*max_scenic_score)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_one_test() -> Result<(), String> {
        let result = part_one()?;
        assert_eq!(1711, result);
        Ok(())
    }

    #[test]
    fn part_two_test() -> Result<(), String> {
        let result = part_two()?;
        assert_eq!(301392, result);
        Ok(())
    }

    #[test]
    fn parse_test_ok() -> Result<(), String> {
        let expected = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
        let actual = parse("123\n456\n789")?;
        assert_eq!(expected, actual);
        Ok(())
    }

    #[test]
    fn parse_test_err() {
        let expected = Err("l is not a digit".into());
        let actual = parse("12l\n456\n789");
        assert_eq!(expected, actual);
    }
}
