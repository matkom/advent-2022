use std::fs::File;
use std::io::{BufRead, BufReader};

const CORRECT_ANSWER: u32 = 895;
const INPUT_PATH: &str = "./input.txt";

#[derive(Debug)]
enum AdventError {
    IOError(String),
    InputError(String),
}

fn main() -> Result<(), AdventError> {
    let mut result = 0;

    let file = File::open(INPUT_PATH).map_err(|err| AdventError::IOError(err.to_string()))?;
    let lines = BufReader::new(file).lines();
    for line in lines {
        let text = line.map_err(|err| AdventError::IOError(err.to_string()))?;

        let ranges: Vec<&str> = text.split(',').collect();
        if ranges.len() != 2 {
            return Err(AdventError::InputError(
                "Line has incorrect number of ranges".to_string(),
            ));
        }

        let (start1, end1) = get_start_end_u32(ranges[0])?;
        let (start2, end2) = get_start_end_u32(ranges[1])?;

        if start1 <= end2 && end1 >= start2 {
            result += 1;
        }
    }

    assert_eq!(CORRECT_ANSWER, result);
    println!("{}", result);
    Ok(())
}

fn get_start_end_u32(range_text: &str) -> Result<(u32, u32), AdventError> {
    let digits_strings: Vec<&str> = range_text.split('-').collect();
    if digits_strings.len() != 2 {
        return Err(AdventError::InputError(
            "Range has incorrect format".to_string(),
        ));
    }

    let start = try_parse_digit(digits_strings[0])?;
    let end = try_parse_digit(digits_strings[1])?;

    Ok((start, end))
}

fn try_parse_digit(digit_string: &str) -> Result<u32, AdventError> {
    digit_string
        .parse()
        .map_err(|_| AdventError::InputError(format!("Cannot convert {} to digit", digit_string)))
}
