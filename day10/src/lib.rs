pub fn part_one() -> Result<i32, String> {
    let instructions = parse()?;
    let cycle_values = get_regx_val_for_each_cycle(&instructions)?;
    let mut result = 0;
    for i in (20..=220).step_by(40) {
        result += i as i32 * cycle_values[i - 1];
    }
    Ok(result)
}

pub fn part_two() -> Result<String, String> {
    let instructions = parse()?;
    let sprite_positions = get_regx_val_for_each_cycle(&instructions)?;

    let mut crt = String::new();
    for (i, sprite_middle_pos) in sprite_positions.iter().enumerate() {
        let sprite_range = (i as i32 % 40) - 1..=(i as i32 % 40) + 1;

        if sprite_range.contains(sprite_middle_pos) {
            crt.push('#');
        } else {
            crt.push('.');
        }

        if (i + 1) % 40 == 0 {
            crt.push('\n');
        }
    }

    Ok(crt)
}

#[derive(Debug)]
enum Instr {
    Noop,
    AddX(i32),
}

impl TryFrom<&str> for Instr {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let instr: Vec<&str> = value.split_whitespace().collect();
        match instr.as_slice() {
            ["noop"] => Ok(Instr::Noop),
            ["addx", num] => {
                let addx_val = num
                    .parse()
                    .map_err(|_| format!("{} is not a valid number", num))?;

                Ok(Instr::AddX(addx_val))
            }
            _ => Err(format!("Unknown instruction {}", value)),
        }
    }
}

fn parse() -> Result<Vec<Instr>, String> {
    let input = include_str!("../input.txt");
    input.lines().map(|line| Instr::try_from(line)).collect()
}

fn get_regx_val_for_each_cycle(instructions: &Vec<Instr>) -> Result<Vec<i32>, String> {
    let mut values = vec![1];
    for instr in instructions {
        let last_val = *values
            .last()
            .ok_or("Instructions vector is empty".to_string())?;

        match instr {
            Instr::Noop => values.push(last_val),
            Instr::AddX(num) => {
                values.push(last_val);
                values.push(last_val + num);
            }
        }
    }

    Ok(values[0..values.len() - 1].to_vec())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() -> Result<(), String> {
        let result = part_one()?;
        assert_eq!(14220, result);
        Ok(())
    }

    #[test]
    fn test_part_two() -> Result<(), String> {
        let result = part_two()?;
        assert_eq!(result, "####.###...##..###..#....####.####.#..#.\n...#.#..#.#..#.#..#.#....#.......#.#..#.\n..#..#..#.#..#.#..#.#....###....#..#..#.\n.#...###..####.###..#....#.....#...#..#.\n#....#.#..#..#.#.#..#....#....#....#..#.\n####.#..#.#..#.#..#.####.#....####..##..\n");
        Ok(())
    }
}
