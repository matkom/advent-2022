use day10::part_two;

fn main() -> Result<(), String> {
    print!("{}", part_two()?);
    Ok(())
}
