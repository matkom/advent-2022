use std::fs::File;
use std::io::{BufRead, BufReader};

const CORRECT_ANSWER: i32 = 13131;
const INPUT_PATH: &str = "./input.txt";

#[derive(Clone, Copy)]
enum Move {
    Rock = 0,
    Paper = 1,
    Scissors = 2,
}

impl TryFrom<char> for Move {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'A' => Ok(Move::Rock),
            'B' => Ok(Move::Paper),
            'C' => Ok(Move::Scissors),
            _ => Err(format!("[ERROR] Unknown move value: {}", value)),
        }
    }
}

#[derive(Clone, Copy)]
enum RoundResult {
    Win = 6,
    Draw = 3,
    Loose = 0,
}

impl TryFrom<char> for RoundResult {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'X' => Ok(RoundResult::Loose),
            'Y' => Ok(RoundResult::Draw),
            'Z' => Ok(RoundResult::Win),
            _ => Err(format!("[ERROR] Unknown round result value: {}", value)),
        }
    }
}

fn main() -> Result<(), String> {
    let mut total_score = 0;

    let file = File::open(INPUT_PATH).expect("[ERROR] Failed reading input file");
    let lines = BufReader::new(file).lines();
    for line in lines {
        let line_chars = get_line_chars(line)?;
        let opponent_move = Move::try_from(line_chars[0])? as i32;
        let expected_result = RoundResult::try_from(line_chars[2])?;
        let your_move = get_your_move(opponent_move, expected_result);
        total_score += expected_result as i32 + your_move + 1;
    }

    assert_eq!(CORRECT_ANSWER, total_score);
    println!("Total score: {}", total_score);
    Ok(())
}

fn get_line_chars(line: Result<String, std::io::Error>) -> Result<Vec<char>, String> {
    let line_text = match line {
        Ok(text) => text,
        Err(err) => return Err(format!("[ERROR] Failed reading line. Error message: {}", err))
    };

    let line_chars: Vec<char> = line_text.chars().collect();
    match line_chars.len() != 3 {
        true => Err("[ERROR] Wrong input".to_string()),
        false => Ok(line_chars)
    }
}

fn get_your_move(opponent_move: i32, expected_result: RoundResult) -> i32 {
    match expected_result {
        RoundResult::Win => (opponent_move + 1) % 3,
        RoundResult::Draw => opponent_move ,
        RoundResult::Loose => (((opponent_move - 1) % 3) + 3) % 3
    }
}
