use std::fs::File;
use std::io::{BufRead, BufReader};

const CORRECT_ANSWER: u32 = 206643;
const INPUT_PATH: &str = "./input.txt";

fn main() {
    let file = File::open(INPUT_PATH).expect("[ERROR] Failed reading input file");
    let lines = BufReader::new(file).lines();

    let mut new_sum = 0;
    let mut calories_sums: Vec<u32> = Vec::new();

    for line in lines {
        let text = line.expect("[ERROR] Failed reading line");

        if text.is_empty() {
            calories_sums.push(new_sum);
            new_sum = 0;
            continue;
        }

        new_sum += text
            .parse::<u32>()
            .expect("[ERROR] What the hell is this input elf?!");
    }

    calories_sums.sort();
    let result = calories_sums.iter().rev().take(3).sum();

    assert_eq!(CORRECT_ANSWER, result);
    println!("Total calories carried by the top three elves: {}", result);
}
