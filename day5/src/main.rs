use std::fs;

const CORRECT_ANSWER: &str = "VLCWHTDSZ";
const INPUT_PATH: &str = "./input.txt";

#[derive(Debug)]
pub enum AdventError {
    IOError(std::io::Error),
    InputError
}

#[derive(Debug)]
pub struct Instruction {
    pub from: usize,
    pub to: usize,
    pub amount: usize
}

type Crate = char;
type CratesStack = Vec<Crate>;

fn main() -> Result<(), AdventError> {
    let input = fs::read_to_string(INPUT_PATH).map_err(|err| AdventError::IOError(err))?;

    let (stacks_str, instr_str) = input.split_once("\n\n").ok_or(AdventError::InputError)?;
    let mut stacks = parse_stacks(stacks_str).ok_or(AdventError::InputError)?;
    let instructions = parse_instructions(instr_str).ok_or(AdventError::InputError)?;

    execute_instructions(&mut stacks, &instructions);
    let answer: String = stacks.iter().filter_map(|stack| stack.iter().last()).collect();

    assert_eq!(CORRECT_ANSWER, answer);
    Ok(())
}

fn parse_stacks(input: &str) -> Option<Vec<CratesStack>> {
    let (stacks_str, stacks_numbers_str) = input.rsplit_once("\n")?;

    let stacks_count_str = stacks_numbers_str.split_whitespace().last()?;
    let stacks_count = stacks_count_str.parse().ok()?;

    let mut stacks = vec![Vec::<Crate>::new(); stacks_count];

    for line in stacks_str.lines().rev() {
        let chars = line.chars().collect::<Vec<char>>();
        for (idx, chunk) in chars.chunks(4).into_iter().enumerate() {
            let second_char = chunk.get(1)?;
            if second_char.is_alphabetic() {
                stacks[idx].push(*second_char);
            }
        }
    }

    Some(stacks)
}

fn parse_instructions(input: &str) -> Option<Vec<Instruction>> {
    let mut instructions = Vec::new();

    for line in input.lines() {
        let rest = line.strip_prefix("move ")?;
        let (amount, rest) = rest.split_once(" from ")?;
        let (from, to) = rest.split_once(" to ")?;

        instructions.push(Instruction {
            amount: amount.parse().ok()?,
            from: from.parse().ok()?,
            to: to.parse().ok()?
        });
    }

    Some(instructions)
}


fn execute_instructions(stacks: &mut Vec<CratesStack>, instructions: &Vec<Instruction>) {
    for instr in instructions {
        let stack = &mut stacks[instr.from - 1];
        let removed_elements = stack.split_off(stack.len() - instr.amount);
        for removed in removed_elements {
            stacks[instr.to - 1].push(removed);
        }
    }
}
