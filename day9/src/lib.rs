use itertools::Itertools;
use std::collections::HashSet;

struct Move {
    direction: Direction,
    distance: i32,
}

#[derive(Debug, Clone, Copy)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl TryFrom<&str> for Direction {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "R" => Ok(Direction::Right),
            "L" => Ok(Direction::Left),
            "U" => Ok(Direction::Up),
            "D" => Ok(Direction::Down),
            _ => Err(format!("{} is not a valid direction", value)),
        }
    }
}

#[derive(Debug, Default, Hash, Eq, PartialEq, Clone, Copy)]
struct Position {
    x: i32,
    y: i32,
}

pub fn part_one() -> Result<usize, String> {
    let moves = parse()?;
    let mut head = Position::default();
    let mut tail = Position::default();

    let mut visited = HashSet::new();
    visited.insert(tail);

    for move_info in moves {
        for _ in 0..move_info.distance {
            move_head(&mut head, move_info.direction);
            if !head_touches_tail(&head, &tail) {
                move_tail(&head, &mut tail);
                visited.insert(tail);
            }
        }
    }

    Ok(visited.len())
}

pub fn part_two() -> Result<usize, String> {
    let moves = parse()?;
    let mut rope = vec![Position::default(); 10];

    if rope.len() != 10 {
        return Err(format!(
            "Rope was not created properly. Expected length is 10, actual {}",
            rope.len()
        ));
    }

    let mut visited = HashSet::new();
    visited.insert(*rope.last().unwrap());

    for move_info in moves {
        for _ in 0..move_info.distance {
            move_head(rope.first_mut().unwrap(), move_info.direction);
            for (head_idx, tail_idx) in (0..rope.len()).tuple_windows() {
                if !head_touches_tail(&rope[head_idx], &rope[tail_idx]) {
                    move_tail(&rope[head_idx].clone(), &mut rope[tail_idx]);
                    if tail_idx == rope.len() - 1 {
                        visited.insert(rope[tail_idx]);
                    }
                }
            }
        }
    }

    Ok(visited.len())
}

fn parse() -> Result<Vec<Move>, String> {
    let input = include_str!("../input.txt");
    let mut moves = vec![];
    for line in input.lines() {
        let (direction_txt, distance_txt) = line.split_once(' ').ok_or("Unknown motion format")?;
        let direction = Direction::try_from(direction_txt)?;
        let distance = distance_txt
            .parse()
            .map_err(|_| format!("{} is not a valid distance", distance_txt))?;

        moves.push(Move {
            direction,
            distance,
        })
    }
    Ok(moves)
}

fn move_head(head: &mut Position, direction: Direction) {
    match direction {
        Direction::Right => head.x += 1,
        Direction::Left => head.x -= 1,
        Direction::Up => head.y += 1,
        Direction::Down => head.y -= 1,
    }
}

fn move_tail(head: &Position, tail: &mut Position) {
    let (x_diff, y_diff) = head_tail_diff(head, tail);
    tail.x += x_diff.signum();
    tail.y += y_diff.signum();
}

fn head_tail_diff(head: &Position, tail: &Position) -> (i32, i32) {
    let x_diff = head.x - tail.x;
    let y_diff = head.y - tail.y;
    (x_diff, y_diff)
}

fn head_touches_tail(head: &Position, tail: &Position) -> bool {
    let (x_diff, y_diff) = head_tail_diff(head, tail);
    x_diff.abs() <= 1 && y_diff.abs() <= 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() -> Result<(), String> {
        let result = part_one()?;
        assert_eq!(6243, result);
        Ok(())
    }

    #[test]
    fn test_part_two() -> Result<(), String> {
        let result = part_two()?;
        assert_eq!(2630, result);
        Ok(())
    }
}
