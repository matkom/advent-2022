#[derive(Debug, Clone)]
struct Monkey {
    items: Vec<u32>,
    op: Op,
    test: Test,
}

#[derive(Debug, Clone)]
enum Op {
    Add(u32),
    Mult(u32),
    Square,
}

#[derive(Debug, Clone)]
struct Test {
    cond: u32,
    true_monkey: usize,
    false_monkey: usize,
}

// TODO Error handling
fn parse(input: &str) -> Vec<Monkey> {
    let mut monkeys = vec![];

    for monkey_txt in input.split("\n\n") {
        let mut lines_iter = monkey_txt.lines().skip(1);

        let items = lines_iter
            .next()
            .unwrap()
            .trim()
            .strip_prefix("Starting items: ")
            .unwrap()
            .split_terminator(", ")
            .map(|item_txt| item_txt.parse::<u32>().unwrap())
            .collect::<Vec<u32>>();

        let op_txt = lines_iter
            .next()
            .unwrap()
            .trim()
            .strip_prefix("Operation: new = ")
            .unwrap()
            .split_whitespace()
            .collect::<Vec<&str>>();

        let op = match *op_txt.get(1).unwrap() {
            "+" => Op::Add(op_txt.get(2).unwrap().parse().unwrap()),
            "*" => {
                let val_txt = op_txt.get(2).unwrap();
                if *val_txt == "old" {
                    Op::Square
                } else {
                    Op::Mult(val_txt.parse().unwrap())
                }
            }
            _ => todo!(),
        };

        let cond = lines_iter
            .next()
            .unwrap()
            .trim()
            .strip_prefix("Test: divisible by ")
            .unwrap()
            .parse()
            .unwrap();

        let true_monkey = lines_iter
            .next()
            .unwrap()
            .trim()
            .strip_prefix("If true: throw to monkey ")
            .unwrap()
            .parse()
            .unwrap();

        let false_monkey = lines_iter
            .next()
            .unwrap()
            .trim()
            .strip_prefix("If false: throw to monkey ")
            .unwrap()
            .parse()
            .unwrap();

        let test = Test {
            cond,
            true_monkey,
            false_monkey,
        };

        monkeys.push(Monkey { items, op, test })
    }

    monkeys
}

// TODO Error handling
pub fn part_one() -> u32 {
    let mut monkeys = parse(include_str!("../input.txt"));
    let mut inspections = vec![0; monkeys.len()];

    for _ in 0..20 {
        for i in 0..monkeys.len() {
            let items: Vec<u32> = monkeys[i].items.drain(..).collect();
            let monkey = monkeys[i].clone();
            for item in items {
                inspections[i] += 1;

                let mut worry_lvl = item;
                match monkey.op {
                    Op::Add(num) => worry_lvl += num,
                    Op::Mult(num) => worry_lvl *= num,
                    Op::Square => worry_lvl *= worry_lvl,
                }
                worry_lvl = worry_lvl / 3;

                let monkey_id = if worry_lvl % monkey.test.cond == 0 {
                    monkey.test.true_monkey
                } else {
                    monkey.test.false_monkey
                };

                monkeys[monkey_id].items.push(worry_lvl);
            }
        }
    }
    inspections.sort_unstable();
    inspections.iter().rev().take(2).product()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one();
        assert_eq!(58794, result);
    }
}
