#![feature(test)]
extern crate test;

#[allow(unused_imports)]
use test::Bencher;
use std::str::Lines;

#[allow(dead_code)]
fn parse(input: &mut Lines) -> Result<Vec<u64>, String> {
    let mut size = 0;
    let mut sub_dir_sizes = vec![];
    loop {
        match input
            .next()
            .map(|s| s.split_whitespace().collect::<Vec<_>>())
            .as_deref()
        {
            Some(["$", "cd", ".."]) | None => break,
            Some(["$", "cd", "/"]) | Some(["$", "ls"]) | Some(["dir", _]) => continue,
            Some(["$", "cd", path]) if *path != "/" => {
                sub_dir_sizes.extend(parse(input)?);
                size += sub_dir_sizes.last().ok_or("Expected subdirectory but found nothing")?;
            }
            Some([size_txt, _]) => {
                size += size_txt.parse::<u64>().map_err(|err| err.to_string())?;
            }
            Some(txt) => return Err(format!("Unknown input {}", txt.join(" ").to_string())),
        }
    }
    sub_dir_sizes.push(size);
    Ok(sub_dir_sizes)
}

#[allow(dead_code)]
fn part_one() -> Result<u64, String> {
    let input = include_str!("../input.txt");
    let dir_sizes = parse(&mut input.lines())?;

    let answer = dir_sizes
        .into_iter()
        .filter(|&size| size <= 100000)
        .sum();

    Ok(answer)
}

#[allow(dead_code)]
fn part_two() -> Result<u64, String> {
    let input = include_str!("../input.txt");
    let dir_sizes = parse(&mut input.lines())?;

    let used_space = dir_sizes.last().ok_or("Empty directories list")?;
    let to_delete = 30000000 - (70000000 - used_space);

    let answer = dir_sizes
        .into_iter()
        .filter(|&size| size >= to_delete)
        .min()
        .ok_or("There is no directory matching deletion criteria")?;

    Ok(answer)
}

#[test]
fn test_part_one() -> Result<(), String> {
    assert_eq!(1086293, part_one()?);
    Ok(())
}

#[test]
fn test_part_two() -> Result<(), String> {
    assert_eq!(366028, part_two()?);
    Ok(())
}

#[bench]
fn benchmark_parsing(b: &mut Bencher) {
    let input = include_str!("../input.txt");
    b.iter(|| parse(&mut input.lines()))
}

#[bench]
fn benchmark_part_one(b: &mut Bencher) {
    b.iter(|| part_one())
}

#[bench]
fn benchmark_part_two(b: &mut Bencher) {
    b.iter(|| part_two())
}
